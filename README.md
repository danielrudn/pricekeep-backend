# PriceKeep Backend #
This is the backend for PriceKeep. Currently it adds items by getting the names and prices, periodically checks the items to see if the price changed, and sends a push notification the item owner if the price did change.

## ** Note: ** The backend is far from finished. ##