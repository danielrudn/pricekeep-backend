import java.util.Map;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Searches a website for item data
 */
public class DataFinder {
	
	private static final Pattern pricePattern = Pattern.compile("[\\$]?[0-9]+\\.[0-9][0-9][\\$]?");

	private DataFinder() { }
	
	public static boolean getItemData(Document doc, String startPrice, Map<String, String> itemMap) {
		// replace elements that cause price to not match the regex 
		doc = Jsoup.parse(doc.toString().replaceAll("<strong>", "").replaceAll("</strong>", "").replaceAll("<sup>", "").replaceAll("</sup>", "").replaceAll("<sub>", "").replaceAll("</sub>", "").replaceAll(",",  ""));
		// try to find element with price
		for(Element e : doc.getElementsMatchingOwnText(pricePattern)) {
			if(e.toString().contains(startPrice)) {
				itemMap.put("currentPrice", startPrice);
				itemMap.put("element", e.toString().substring(0, e.toString().indexOf(">")+1));
				break;
			}
		} 
		// Check schemas to see if we have matches, we do this last since sometimes the meta tags and itemprop tags don't always have the latest price
		for(Element e: doc.getElementsByAttribute("itemprop")) {
			if(e.attr("itemprop").equalsIgnoreCase("price")) {
				if(e.attr("content").matches(pricePattern.toString()) && !itemMap.containsKey("currentPrice")) {
					itemMap.put("currentPrice", e.attr("content"));
					itemMap.put("element", e.toString().substring(0, e.toString().indexOf(">")+1));
				} else if(e.ownText().equals(startPrice)) {
					itemMap.put("currentPrice", e.ownText());
					itemMap.put("element", e.toString().substring(0, e.toString().indexOf(">")+1));
				}
			} else if(e.attr("itemprop").equalsIgnoreCase("name") && !itemMap.containsKey("name")) {
				if(!e.attr("content").equals(" ") && !e.attr("content").equals("")) {
					itemMap.put("name", e.attr("content"));
				} //else { 
					//System.out.println("itemprop=name: " + 	e.ownText());
				//	itemMap.put("name", e.ownText());
			//}
			} else if(e.attr("itemprop").equalsIgnoreCase("image") && !itemMap.containsKey("imgURL")) {
				if(!e.attr("content").equals(" ") && !e.attr("content").equals("")) {
					itemMap.put("imgURL", e.attr("content"));
				} else if (e.hasAttr("src")) { 
					itemMap.put("imgURL", e.attr("src"));
				} else {
					itemMap.put("imgURL", e.ownText());
				}
				// put item name
				if(e.hasAttr("alt") && !itemMap.containsKey("name")) {
					itemMap.put("name", e.attr("alt"));
				}
			}
		}
		// try to get image url 
		for(Element e : doc.getElementsByTag("meta")) {
			if(e.attr("property").toLowerCase().contains("image") || e.attr("property").toLowerCase().contains("img")) {
				itemMap.put("imgURL", e.attr("content"));
				break;
			}
		}
		// getting image didn't work, try to get it from image tag
		if(!itemMap.containsKey("imgURL")) {
			for(Element e : doc.getElementsByTag("img"))
			{	
				if(e.attr("alt").length() >= 7 && doc.title().contains(e.attr("alt").substring(e.attr("alt").length() / 2, e.attr("alt").length() / 2 + 4))
						&& e.attr("src").length() < 150)
				{
					itemMap.put("imgURL", e.attr("src"));
					itemMap.put("name", e.attr("alt"));
					break;
				}
			}
		}
		// Set item name as doc title if we haven't found it yet
		if(!itemMap.containsKey("name")) {
			itemMap.put("name", doc.title());
		}
		return isValidItem(itemMap);
	}
	
	public static boolean getItemDataTest(Document doc, String startPrice, Map<String, String> itemMap) {
		// replace elements that cause price to not match the regex 
		doc = Jsoup.parse(doc.toString().replaceAll("<strong>", "").replaceAll("</strong>", "").replaceAll("<sup>", "").replaceAll("</sup>", "").replaceAll("<sub>", "").replaceAll("</sub>", "").replaceAll(",",  ""));
		// Check schemas to see if we have matches, we do this last since sometimes the meta tags and itemprop tags don't always have the latest price
		for(Element e: doc.getElementsByAttribute("itemprop")) {
			if(e.attr("itemprop").equalsIgnoreCase("price")) {
				if(e.attr("content").matches(pricePattern.toString()) && !itemMap.containsKey("currentPrice")) {
					itemMap.put("currentPrice", e.attr("content"));
					itemMap.put("element", e.toString().substring(0, e.toString().indexOf(">")+1));
				} else if(e.ownText().equals(startPrice)) {
					itemMap.put("currentPrice", e.ownText());
					itemMap.put("element", e.toString().substring(0, e.toString().indexOf(">")+1));
				}
			} else if(e.attr("itemprop").equalsIgnoreCase("name") && !itemMap.containsKey("name")) {
				if(!e.attr("content").equals(" ") && !e.attr("content").equals("")) {
					itemMap.put("name", e.attr("content"));
				}
			} else if(e.attr("itemprop").equalsIgnoreCase("image") && !itemMap.containsKey("imgURL")) {
				if(!e.attr("content").equals(" ") && !e.attr("content").equals("")) {
					itemMap.put("imgURL", e.attr("content"));
				} else if (e.hasAttr("src")) { 
					itemMap.put("imgURL", e.attr("src"));
				} else {
					itemMap.put("imgURL", e.ownText());
				}
				// put item name
				if(e.hasAttr("alt") && !itemMap.containsKey("name")) {
					itemMap.put("name", e.attr("alt"));
				}
			}
		}
		// try to find element with price
		for(Element e : doc.getElementsMatchingOwnText(pricePattern)) {
			if(e.toString().contains(startPrice)) {
				itemMap.put("currentPrice", startPrice);
				itemMap.put("element", e.toString().substring(0, e.toString().indexOf(">")+1));
				break;
			}
		} 
		// try to get image url 
		for(Element e : doc.getElementsByTag("meta")) {
			if(e.attr("property").toLowerCase().contains("image") || e.attr("property").toLowerCase().contains("img")) {
				itemMap.put("imgURL", e.attr("content"));
				break;
			}
		}
		// getting image didn't work, try to get it from image tag
		if(!itemMap.containsKey("imgURL")) {
			for(Element e : doc.getElementsByTag("img"))
			{	
				if(e.attr("alt").length() >= 7 && doc.title().contains(e.attr("alt").substring(e.attr("alt").length() / 2, e.attr("alt").length() / 2 + 4))
						&& e.attr("src").length() < 150)
				{
					itemMap.put("imgURL", e.attr("src"));
					itemMap.put("name", e.attr("alt"));
					break;
				}
			}
		}
		// Set item name as doc title if we haven't found it yet
		if(!itemMap.containsKey("name")) {
			itemMap.put("name", doc.title());
		}
		return isValidItem(itemMap);
	}
	
	private static boolean isValidItem(Map<String, String> itemMap) {
		System.out.println("ItemMap:\n" + itemMap.toString());
		if(itemMap.containsKey("imgURL") && itemMap.containsKey("name") && itemMap.containsKey("currentPrice") && itemMap.containsKey("element")) {
			System.out.println("returned true");
			return true;
		}
		System.out.println("returned false");
		return false;
	}
	
}