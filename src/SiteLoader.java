import java.io.IOException;
import java.util.logging.Level;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Loads the text version of a given website
 */
public class SiteLoader {
	
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
	private static final int TIMEOUT = 4000;
	private static final int NORMAL_CONNECT_ATTEMPT = 0, CANONICAL_CONNECT_ATTEMPT = 1, JAVASCRIPT_CONNECT_ATTEMPT = 2;
	private static final int MAX_CONNECTION_ATTEMPTS = 2;
	
	private String previousURL = "", newURL = "";
	private Document doc;
	private WebClient client;
	private int numAttempts = 0;
	private boolean setConnectionType = false;
	
	public SiteLoader() {
		// initialize web client
		try { 
		client = new WebClient(BrowserVersion.FIREFOX_38);
		client.setAjaxController(new NicelyResynchronizingAjaxController());
		client.getOptions().setPopupBlockerEnabled(false);
		client.getOptions().setCssEnabled(false);
		client.getOptions().setAppletEnabled(false);
		client.getOptions().setJavaScriptEnabled(true);
		client.getOptions().setThrowExceptionOnScriptError(false);
		client.getOptions().setThrowExceptionOnFailingStatusCode(false);
		// turn off warnings
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
		} catch (Exception e) { }
	}
	
	/**
	 * Tries to connect to a website and each time the same url is passed, a different method of connection is used
	 * @param url
	 * @return HTML source of a website
	 * @throws IOException 
	 */
	public Document loadSite(String url) throws IOException {
		if(previousURL.equals(url)) {
			numAttempts++;
		} else if(!setConnectionType) {
			numAttempts = 0;
		}
		this.previousURL = url;
		this.newURL = url;
		
		if(numAttempts == SiteLoader.NORMAL_CONNECT_ATTEMPT) {
			doc = Jsoup.connect(url).userAgent(USER_AGENT).followRedirects(true).timeout(TIMEOUT).get();
			return doc;
		} else if(numAttempts == SiteLoader.CANONICAL_CONNECT_ATTEMPT) {
			for(Element e : doc.getElementsByTag("link")) {
				if(e.attr("rel").equalsIgnoreCase("canonical") && e.attr("href").contains("http")) {
					newURL = e.attr("href");
					doc = Jsoup.connect(newURL).userAgent(USER_AGENT).followRedirects(true).timeout(TIMEOUT).get();
					return doc;
				}
			}
			// didn't find canonical url, try to load again
			return loadSite(url);
		} else if(numAttempts == SiteLoader.JAVASCRIPT_CONNECT_ATTEMPT) {
			HtmlPage page =  client.getPage(url);
			client.waitForBackgroundJavaScriptStartingBefore(3000);
			doc = Jsoup.parse(page.asXml());
			return doc;
		}
		
		return null;
	}
	
	/**
	 * Sets the connection type for the NEXT connection attempt
	 */
	public void setConnectionType(int type) {
		this.numAttempts = type;
		setConnectionType = true;
	}
	
	/**
	 * Gets the connection type of the LAST connection attempt
	 * @return The connection type of the LAST connection attempt
	 */
	public int getConnectionType() {
		return numAttempts;
	}
	
	public String getNewURL() {
		return newURL;
	}
	
	public boolean hasAttempts() {
		setConnectionType = false;
		if(numAttempts <= SiteLoader.MAX_CONNECTION_ATTEMPTS) {
			return true;
		}
		return false;
	}
	

}