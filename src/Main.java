import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class Main implements ValueEventListener{
	
	private static Firebase ref;
	
	// Constants for times in milliseconds.
	private static final int TEN_MINUTES = 600000;// THIRTY_MINUTES = 3 * TEN_MINUTES, HOUR = 2 * THIRTY_MINUTES, TWELVE_HOUR = 12 * HOUR, DAY = 2 * TWELVE_HOUR;
	private static final Pattern pricePattern = Pattern.compile("[\\$]?[0-9]+\\.[0-9][0-9][\\$]?");
	
	private static Calendar cal = Calendar.getInstance();
	private static SiteLoader siteLoader;
	
	public static void main(String[] args) {
		siteLoader = new SiteLoader();
		ref = new Firebase("https://resplendent-inferno-4301.firebaseio.com");
		ref.child("addQueue/").addValueEventListener(new Main());
		long startTime = System.currentTimeMillis();
		long currentTime = 0, elapsedTime = 0;
		int timesTenMinutesPassed = 0;
		checkPricesTest("12 hours");
		while(true) { 
			currentTime = System.currentTimeMillis();
			elapsedTime = currentTime - startTime;
			if(elapsedTime >= TEN_MINUTES) { // 10 minutes passed
				elapsedTime=0;
				startTime = System.currentTimeMillis();
				timesTenMinutesPassed++;
				if(timesTenMinutesPassed % (6*24) == 0) {
					checkPricesTest("day");
				} else if(timesTenMinutesPassed % (6*12) == 0) {
					checkPricesTest("12 hours");
				} else if(timesTenMinutesPassed % 6 == 0) {
					checkPricesTest("hour");
				} else if(timesTenMinutesPassed % 3 == 0) {
					checkPricesTest("30 minutes");
				} 
			}
		}
	}
	
	// TODO: save connection attempt type in update queue to use it right away, different way of checking elements rather than e.contains(element) since it can have an attribute with price i.e <span content="19.99">
	public static void checkPrices(String interval) {
		System.out.println("Checking for " + interval);
		ref.child("updateQueue/" + interval).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot data) {
				for(DataSnapshot item : data.getChildren()) {
					try {
						System.out.println("======================================================================================================================");
						System.out.println("CHECKING URL: " + item.child("url").getValue().toString());
						System.out.println("currentPrice: " + item.child("currentPrice").getValue().toString() + " element: " + item.child("element").getValue().toString());
						boolean foundPrice = false;
						while(siteLoader.hasAttempts() && !foundPrice) {
							if(item.hasChild("connectionType")) {
								siteLoader.setConnectionType(Integer.parseInt(item.child("connectionType").getValue().toString()));
							}
							Document doc = siteLoader.loadSite(item.child("url").getValue().toString());
							String element = item.child("element").getValue().toString();
							int index = (element.indexOf(" ") == -1) ? element.indexOf(">") : element.indexOf(" ");
							System.out.println("Checking " + doc.getElementsByTag(element.substring(1, index)).size() + " " + element.substring(1, index) + " elements");
							for(Element e : doc.getElementsByTag(element.substring(1, index))) {
								if(e.toString().contains(element)) {
									Matcher matcher = pricePattern.matcher(e.ownText());
									String currentPrice = "";
									if(matcher.find()) {
										currentPrice = matcher.group().replaceAll("\\$", "");
										foundPrice = true;
										// update date checked
										ref.child("users/" + item.child("uid").getValue().toString() + "/items/" + item.getKey().toString() + "/dateChecked").setValue(Long.toString(cal.getTimeInMillis()));
										System.out.println("CurrentPrice: " + currentPrice);
										// replace if it's different from start price
										if(!currentPrice.equals("") && !currentPrice.equals(item.child("currentPrice").getValue().toString())) {
											// update current price in users item list and in update queue
											item.child("currentPrice").getRef().setValue(currentPrice);
											ref.child("users/" + item.child("uid").getValue().toString() + "/items/" + item.getKey().toString() + "/currentPrice").setValue(currentPrice);
											if(!item.hasChild("notify") || item.child("notify").getValue().toString().equals("true")) {
												// add notification
												ref.child("users/" + item.child("uid").getValue().toString() + "/notifications/" + item.getKey().toString()).setValue(currentPrice);
											}
										}
										break;
									}
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
				
			@Override
			public void onCancelled(FirebaseError err) {
				System.out.println("OnCancelled() Firebase Error:\n" + err.toString());
			}
				
			});
	}	
		
	public static void checkPricesTest(String interval) {
		System.out.println("Checking for " + interval);
		ref.child("updateQueue/" + interval).addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot data) {
				for(DataSnapshot item : data.getChildren()) {
					try {
						while(siteLoader.hasAttempts()) {
							if(item.hasChild("connectionType")) {
								int type = Integer.parseInt(item.child("connectionType").getValue().toString());
								// TEMP
								if(type == 1) {
									type = 0;
								}
								siteLoader.setConnectionType(type);
							}
							Document doc = siteLoader.loadSite(item.child("url").getValue().toString());
							// replace elements that cause price to not match the regex 
							doc = Jsoup.parse(doc.toString().replaceAll("<strong>", "").replaceAll("</strong>", "").replaceAll("<sup>", "").replaceAll("</sup>", "").replaceAll("<sub>", "").replaceAll("</sub>", "").replaceAll(",",  ""));
							String element = item.child("element").getValue().toString();
							int index = doc.toString().indexOf(element);
							if(index == -1) {
								System.out.println("couldn't find element: " + element);
								continue;
							}
							Matcher matcher = pricePattern.matcher(doc.toString().substring(index));
							String currentPrice = "";
							if(matcher.find()) {
								currentPrice = matcher.group().replaceAll("\\$", "");
								System.out.println("currentPrice: " + currentPrice);
								// update date checked
								ref.child("users/" + item.child("uid").getValue().toString() + "/items/" + item.getKey().toString() + "/dateChecked").setValue(Long.toString(cal.getTimeInMillis()));
								// replace if it's different from start price
								if(!currentPrice.equals("") && !currentPrice.equals(item.child("currentPrice").getValue().toString())) {
									// update current price in users item list and in update queue
									item.child("currentPrice").getRef().setValue(currentPrice);
									ref.child("users/" + item.child("uid").getValue().toString() + "/items/" + item.getKey().toString() + "/currentPrice").setValue(currentPrice);
									if(item.child("notify").getValue().toString().equals("true")) {
										// add notification
										ref.child("users/" + item.child("uid").getValue().toString() + "/notifications/" + item.getKey().toString()).setValue(currentPrice);
									}
								}
								break;
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			@Override
			public void onCancelled(FirebaseError err) {
				System.out.println("OnCancelled() Firebase Error:\n" + err.toString());
			}
			
		});
	}
		
		
	@Override
	// Listens in the addQueue for items and then moves them into updateQueue and users item list.
	public void onDataChange(DataSnapshot data) {
		for (DataSnapshot item : data.getChildren()) {
			try {
				System.out.println("Adding URL " + item.child("url").getValue());
				String startPrice = item.child("startPrice").getValue().toString();
				String url = item.child("url").getValue().toString();
				
				// remove mobile
				if(url.contains("m.") && url.contains("/?lang=")) {
					url = url.replace("m.", "").replaceAll("/\\?lang=[a-z][a-z]\\#", "");
					System.out.println("newURL: " + url);
				}
				// create item map and add info we already have
				Map<String, String> itemMap = new HashMap<String, String>();
				itemMap.put("startPrice", startPrice);
				itemMap.put("updateInterval", item.child("updateInterval").getValue().toString());
				itemMap.put("url", url);
				itemMap.put("notify", "true");
				itemMap.put("dateAdded", Long.toString(cal.getTimeInMillis()));
				itemMap.put("dateChecked", Long.toString(cal.getTimeInMillis()));
				// connect to site and get data
				while(siteLoader.hasAttempts()) {
					Document doc = siteLoader.loadSite(url);
					if(DataFinder.getItemData(doc, startPrice, itemMap)) {
						// Add item to users item list
						ref.child("users/" + item.child("uid").getValue().toString() + "/items/" + item.getKey().toString()).setValue(itemMap);
						// Add to update queue
						// Also remove all elements that we dont need in the update queue
						itemMap.put("connectionType", Integer.toString(siteLoader.getConnectionType()));
						// put updated url, the one used to connection and get the price
						itemMap.put("url", siteLoader.getNewURL());
						itemMap.put("uid", item.child("uid").getValue().toString());
						itemMap.remove("updateInterval");
						itemMap.remove("startPrice");
						itemMap.remove("imgURL");
						itemMap.remove("name");
						itemMap.remove("dateAdded");
						itemMap.remove("dateChecked");
						ref.child("updateQueue/" + item.child("updateInterval").getValue().toString() + "/" + item.getKey().toString()).setValue(itemMap);
						break;
					}
				}
				// Remove this item from the add queue
				item.getRef().removeValue();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onCancelled(FirebaseError err) {
		System.out.println("Firebase Error:\n" + err.toString());
	}
	
}